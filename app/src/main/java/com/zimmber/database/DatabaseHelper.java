package com.zimmber.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.zimmber.models.DotEntity;

/**
 * Created by karan on 2/6/16.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String FLOAT_TYPE = " REAL ";
    private static final String COMMA_SEP = " , ";
    private static final String INT_TYPE = " INTEGER ";
    private static final String PRI_KEY = " PRIMARY KEY ";

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Zimbber.db";

    private Context mContext;
    private static DatabaseHelper mInstance;

    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.mContext = context;
    }

    /**
     * get the lazy loaded singleton instance of this class
     * */
    public synchronized static DatabaseHelper getInstance(Context context) throws NullPointerException{
        if(mInstance==null){
            if (context == null){
                throw new NullPointerException("Context cannot be null");
            }
            mInstance = new DatabaseHelper(context);
        }
        return mInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("creating ", " database");
        db.execSQL(getCreateQueries());
        Log.d("created ", " database");
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(getDeleteQueries());
        onCreate(db);
    }

    /**
     * get the sql query to be executed on database create
     * */
    private String getCreateQueries(){
        String createQuery = "CREATE TABLE " + DotEntity.DotColumns.TABLE_NAME + " (" +
                DotEntity.DotColumns._ID + INT_TYPE + PRI_KEY + COMMA_SEP +
                DotEntity.DotColumns.COLOR + INT_TYPE + COMMA_SEP +
                DotEntity.DotColumns.RADIUS + INT_TYPE + COMMA_SEP +
                DotEntity.DotColumns.LOC_X + FLOAT_TYPE + COMMA_SEP +
                DotEntity.DotColumns.LOC_Y + FLOAT_TYPE +
                " )";
        return createQuery;
    }

    /**
     * get the sql query to be executed on database delete
     * */
    private String getDeleteQueries(){
        String deleteQuery = "DROP TABLE IF EXISTS " + DotEntity.DotColumns.TABLE_NAME;
        return deleteQuery;
    }
}
