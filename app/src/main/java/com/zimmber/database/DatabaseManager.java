package com.zimmber.database;

import android.database.Cursor;
import android.os.AsyncTask;

import com.zimmber.config.constants.Errors;
import com.zimmber.controllers.interfaces.DBCallback;
import com.zimmber.models.BaseDBEntity;
import com.zimmber.ui.interfaces.VoidCallback;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by karan on 2/6/16.
 */
public class DatabaseManager<T extends BaseDBEntity> {

    private DatabaseHelper mDBHelper;
    private Class<T> mClass;

    public DatabaseManager(DatabaseHelper mDBHelper, Class<T> mClass) {
        this.mDBHelper = mDBHelper;
        this.mClass = mClass;
    }

    /**
     * insert an object of instance T in the database (not exposed to the controller layer)
     * @param object the object to be inserted
     * @return id of the object inserted
     * */
    private long insert(T object){
        return mDBHelper.getWritableDatabase()
                .insert(object.getTableName(),null,object.getContent());
    }

    /**
     * asynchronously insert an object into the database (exposed to the controller layer)
     * @param object the object to be inserted
     * @param dbCallback callback at controller layer
     * */
    public void insert(final T object, final DBCallback<Long> dbCallback){
        new AsyncTask<Void,Void,Long>(){

            @Override
            protected Long doInBackground(Void... voids) {
                return insert(object);
            }

            @Override
            protected void onPostExecute(Long aLong) {
                super.onPostExecute(aLong);
                if (dbCallback!=null){
                    if (aLong!=null)
                        dbCallback.onSuccess(aLong);
                    else
                        dbCallback.onFail(Errors.ERROR_INSERT);
                }
            }
        }.execute();
    }

    /**
     * find all the elements of any database entity by descending order by _id param (not exposed to the controller layer)
     * @return List of all objects of instance T
     * */
    private List<T> findAll(){
        T entity = null;
        try {
            entity = mClass.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
            return null;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }

        Cursor c = mDBHelper.getReadableDatabase().query(
                entity.getTableName(),
                entity.getProjections(),
                null,
                null,
                null,
                null,
                null
        );

        return parseCursorForList(c);
    }

    /**
     * find all objects in database of instance T (exposed to the controller layer)
     * @param dbCallback callback at controller layer
     * */
    public void findAll(final DBCallback<List<T>> dbCallback){
        new AsyncTask<Void,Void,List<T>>(){

            @Override
            protected List<T> doInBackground(Void... voids) {
                return findAll();
            }

            @Override
            protected void onPostExecute(List<T> ts) {
                super.onPostExecute(ts);
                if (ts!=null)
                    dbCallback.onSuccess(ts);
                else
                    dbCallback.onFail(Errors.ERROR_LOAD);
            }

        }.execute();
    }

    /**
     * Execute a raw sql query (not exposed to the controller layer)
     * @param query the query to be executed
     *
     * */
    private void execRawQuery(String query){
        mDBHelper.getWritableDatabase().execSQL(query);
    }

    /**
     * Execute a raw sql query
     * @param query the query to be executed
     * @param voidCallback callback at the controller layer
     * */
    public void execRawQuery(final String query, final VoidCallback voidCallback){
        new AsyncTask<Void,Void,Void>(){
            @Override
            protected Void doInBackground(Void... voids) {
                execRawQuery(query);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                voidCallback.onResponse();
            }
        }.execute();
    }

    /**
     * @return list of Database Entity after parsing from
     * @param c
     * */
    private List<T> parseCursorForList(Cursor c){
        List<T> list = new ArrayList<>();
        T entity = null;
        try {
            entity = mClass.newInstance();
        } catch (InstantiationException e) {
            return null;
        } catch (IllegalAccessException e) {
            return null;
        }
        if (c.moveToFirst()){
            do {
                list.add((T) entity.getObjectFromCursor(c));
            } while (c.moveToNext());
        }

        c.close();
        return list;
    }

}
