package com.zimmber.models;

import android.content.ContentValues;
import android.database.Cursor;
import android.provider.BaseColumns;

import com.zimmber.ui.views.DrawingView;

/**
 * Created by karan on 2/6/16.
 */
public class DotEntity extends BaseDBEntity {

    private int color;
    private int radius;
    private float locX;
    private float locY;

    public DotEntity(int color, int radius, float locX, float locY) {
        this.color = color;
        this.radius = radius;
        this.locX = locX;
        this.locY = locY;
    }

    public DotEntity() {
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public float getLocX() {
        return locX;
    }

    public void setLocX(float locX) {
        this.locX = locX;
    }

    public float getLocY() {
        return locY;
    }

    public void setLocY(float locY) {
        this.locY = locY;
    }

    public static class DotColumns implements BaseColumns{
        public static final String TABLE_NAME = "DOT";
        public static final String COLOR = "color";
        public static final String RADIUS = "radius";
        public static final String LOC_X = "loc_x";
        public static final String LOC_Y = "loc_y";
    }

    @Override
    public String getTableName() {
        return DotColumns.TABLE_NAME;
    }

    @Override
    public ContentValues getContent() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DotColumns.COLOR,this.color);
        contentValues.put(DotColumns.RADIUS,this.radius);
        contentValues.put(DotColumns.LOC_X,this.locX);
        contentValues.put(DotColumns.LOC_Y,this.locY);
        return contentValues;
    }

    @Override
    public DotEntity getObjectFromCursor(Cursor cursor) {
        return new DotEntity(
                cursor.getInt(cursor.getColumnIndexOrThrow(DotColumns.COLOR)),
                cursor.getInt(cursor.getColumnIndexOrThrow(DotColumns.RADIUS)),
                cursor.getFloat(cursor.getColumnIndexOrThrow(DotColumns.LOC_X)),
                cursor.getFloat(cursor.getColumnIndexOrThrow(DotColumns.LOC_Y))
        );
    }

    @Override
    public String[] getProjections() {
        return new String[]{
                DotColumns.COLOR,
                DotColumns.RADIUS,
                DotColumns.LOC_X,
                DotColumns.LOC_Y
        };
    }
}
