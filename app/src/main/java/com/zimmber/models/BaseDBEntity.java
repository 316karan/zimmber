package com.zimmber.models;

import android.content.ContentValues;
import android.database.Cursor;

/**
 * Created by karan on 2/6/16.
 */
public abstract class BaseDBEntity {

    /**
     * @return the name of the table of the Database Entity
     * */
    public abstract String getTableName();

    /**
     * @return the content values of the object while inserting in the table
     * */
    public abstract ContentValues getContent();

    /**
     * @return the database entity mapped from a cursor
     * */
    public abstract BaseDBEntity getObjectFromCursor(Cursor cursor);

    /**
     * @return the projections of all columns in the database entity
     * */
    public abstract String[] getProjections();

}
