package com.zimmber.utils;

import android.graphics.Color;

import com.zimmber.config.AppController;
import com.zimmber.config.constants.StaticVars;
import com.zimmber.config.constants.StorageVars;

/**
 * Created by karan on 2/6/16.
 */
public class Utils {

    /**
     * get the last selected color from the shared preferences
     * @return String of last selected color
     * */
    public static String getLastSelectedColor(){
        return AppController.getSessionManager().getStringPref(StorageVars.LAST_SELECTED_COLOR, StaticVars.COLORS[0]);
    }

    /**
     * get the last selected int value of the color
     * @return int value of the last selected color
     * */
    public static int getLastSelectedColorInt(){
        return Color.parseColor(getLastSelectedColor());
    }
}
