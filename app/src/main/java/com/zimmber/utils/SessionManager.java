package com.zimmber.utils;

/**
 * Created by karan on 6/12/15.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.zimmber.config.constants.StaticVars;

import java.util.Map;

/**
 * Created by karan on 22/8/15.
 */
public class SessionManager {

    private Context context;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private static SessionManager instance;

    private SessionManager(Context context) {
        this.context = context;
        pref = this.context.getSharedPreferences(StaticVars.PREF_NAME, Context.MODE_PRIVATE);
        editor = pref.edit();
    }

    public synchronized static SessionManager getInstance(Context context) {
        if (instance == null) {
            instance = new SessionManager(context);
        }
        return instance;
    }

    public boolean contains(String Key) {
        try {
            return pref.contains(Key);
        } catch (Exception e) {
            
            e.printStackTrace();
        }
        return false;
    }

    public void insertStringPref(String key, String value) {
        try {
            editor.putString(key, value);
            editor.commit();

        } catch (Exception e) {
            
            e.printStackTrace();
        }
    }

    public void removeKey(String key) {
        try {
            editor.remove(key);
            editor.commit();

        } catch (Exception e) {
            
            e.printStackTrace();
        }
    }

    public String getStringPref(String key,String def) {
        try {
            if (this.contains(key))
                return pref.getString(key, def);
        } catch (Exception e) {
            
            e.printStackTrace();
        }
        return def;
    }

    public void all() {
        Map<String, ?> keys = pref.getAll();
        Log.d("logging","whole session");
        for (Map.Entry<String, ?> entry : keys.entrySet()) {
            try {
                Log.d("map values", entry.getKey() + ": "
                        + entry.getValue().toString());
            } catch (Exception e) {
                e.printStackTrace();
                Log.d("null entry",entry.getKey());
            }
        }
    }

    public void clearSF() {
        try {
            editor.clear().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}