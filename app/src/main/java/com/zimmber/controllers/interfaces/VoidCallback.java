package com.zimmber.controllers.interfaces;

/**
 * Created by karan on 2/6/16.
 */
public interface VoidCallback {
    /**
     * called on failure of controller layer method
     * */
    void onResponse();
    /**
     * called on failure of database layer method
     * @param reason reason for failure
     * */
    void onFail(String reason);
}
