package com.zimmber.controllers.interfaces;

/**
 * Created by karan on 2/6/16.
 */
public interface DBCallback<T> {
    /**
     * called on success of db layer method
     * @param response response from database
     * */
    void onSuccess(T response);
    /**
     * called on failure of database layer method
     * @param reason reason for failure
     * */
    void onFail(String reason);
}
