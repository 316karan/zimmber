package com.zimmber.controllers;

import com.zimmber.config.AppController;
import com.zimmber.controllers.interfaces.DBCallback;
import com.zimmber.database.DatabaseManager;
import com.zimmber.models.DotEntity;
import com.zimmber.ui.interfaces.ControllerCallback;
import com.zimmber.ui.interfaces.VoidCallback;

import java.util.List;

/**
 * Created by karan on 2/6/16.
 */
public class DotController {

    private static DotController instance;
    private DatabaseManager<DotEntity> mDBManager;

    private DotController() {
        mDBManager = new DatabaseManager<>(AppController.getDBHelper(),DotEntity.class);
    }

    /**
     * get lazy loaded singleton instance of this class
     * */
    public synchronized static DotController getInstance() {
        if (instance == null)
            instance = new DotController();
        return instance;
    }

    /**
     * creates new dot entity in database
     * @param dot new object to be created
     * @param voidCallback callback at view layer
     * */
    public void createDot(DotEntity dot, final VoidCallback voidCallback){
        mDBManager.insert(dot, new DBCallback<Long>() {
            @Override
            public void onSuccess(Long response) {
                voidCallback.onResponse();
            }

            @Override
            public void onFail(String reason) {
                voidCallback.onFail(reason);
            }
        });
    }

    /**
     * @param controllerCallback callback at view layer
     * */
    public void getAllDots(final ControllerCallback<List<DotEntity>> controllerCallback){
        mDBManager.findAll(new DBCallback<List<DotEntity>>() {
            @Override
            public void onSuccess(List<DotEntity> response) {
                controllerCallback.onResponse(response);
            }

            @Override
            public void onFail(String reason) {
                controllerCallback.onFail(reason);
            }
        });
    }

    /**
     * delete the last dot from database
     * @param voidCallback callback at layer view
     * */
    public void deleteLastDot(final VoidCallback voidCallback){
        mDBManager.execRawQuery(
                "Delete from " + DotEntity.DotColumns.TABLE_NAME + " where " + DotEntity.DotColumns._ID + " = (Select MAX(" + DotEntity.DotColumns._ID + ") from " + DotEntity.DotColumns.TABLE_NAME + ");",
                new VoidCallback() {
                    @Override
                    public void onResponse() {
                        voidCallback.onResponse();
                    }

                    @Override
                    public void onFail(String reason) {
                        voidCallback.onFail("Could not perform undo action");
                    }
                }
        );
    }

    /**
     * delete all dots from database
     * @param voidCallback callback at layer view
     * */
    public void resetDots(final VoidCallback voidCallback){
        mDBManager.execRawQuery(
                "Delete from " + DotEntity.DotColumns.TABLE_NAME + ";",
                new VoidCallback() {
                    @Override
                    public void onResponse() {
                        voidCallback.onResponse();
                    }

                    @Override
                    public void onFail(String reason) {
                        voidCallback.onFail("Could not perform undo action");
                    }
                }
        );
    }

}
