package com.zimmber.ui.interfaces;

/**
 * Created by karan on 2/6/16.
 */
public interface VoidCallback {
    /**
     * called on success of controller layer method
     * */
    void onResponse();
    /**
     * called on failure of controller layer method
     * @param reason reason for failure
     * */
    void onFail(String reason);
}
