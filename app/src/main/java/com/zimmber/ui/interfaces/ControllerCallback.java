package com.zimmber.ui.interfaces;

/**
 * Created by karan on 2/6/16.
 */
public interface ControllerCallback<T> {

    /**
     * called on success of controller layer method
     * @param response response from controller
     * */
    void onResponse(T response);

    /**
     * called on failure of controller layer method
     * @param reason reason for failure
     * */
    void onFail(String reason);

}
