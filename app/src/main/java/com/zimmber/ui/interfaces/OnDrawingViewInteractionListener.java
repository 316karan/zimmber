package com.zimmber.ui.interfaces;

import com.zimmber.models.DotEntity;

/**
 * Created by karan on 2/6/16.
 */
public interface OnDrawingViewInteractionListener {
    /**
     * callback to the above layer when a new dot is added on the screen
     * @param dot POJO of the new dot added
     * */
    void onNewDotDrawn(DotEntity dot);

}
