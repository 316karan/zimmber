package com.zimmber.ui.adapters;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.zimmber.R;
import com.zimmber.config.AppController;
import com.zimmber.config.constants.StaticVars;
import com.zimmber.config.constants.StorageVars;
import com.zimmber.utils.Utils;

/**
 * Created by karan on 2/6/16.
 */
public class ColorListAdapter extends RecyclerView.Adapter<ColorListAdapter.ColorViewHolder>{


    private OnColorItemClickListener mListener;

    public ColorListAdapter(OnColorItemClickListener mListener) {
        this.mListener = mListener;
    }

    @Override
    public int getItemCount() {
        return StaticVars.COLORS.length;
    }

    @Override
    public void onBindViewHolder(ColorViewHolder holder, int position) {
        holder.viewColor.setBackgroundColor(Color.parseColor(StaticVars.COLORS[position]));
        if (StaticVars.COLORS[position].equals(Utils.getLastSelectedColor())){
            holder.ivSelected.setVisibility(View.VISIBLE);
        }else{
            holder.ivSelected.setVisibility(View.GONE);
        }
        holder.container.setTag(position);
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String color = StaticVars.COLORS[(int) view.getTag()];
                AppController.getSessionManager().insertStringPref(StorageVars.LAST_SELECTED_COLOR, color);
                mListener.onColorChosen(Color.parseColor(color));
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public ColorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ColorViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_color,parent,false));
    }

    public static class ColorViewHolder extends RecyclerView.ViewHolder{

        public View viewColor,container;
        public ImageView ivSelected;

        public ColorViewHolder(View itemView) {
            super(itemView);
            container = itemView;
            viewColor = itemView.findViewById(R.id.view_color);
            ivSelected = (ImageView) itemView.findViewById(R.id.iv_selected);
        }

    }

    public interface OnColorItemClickListener {
        /**
         * called when a user selects a color
         * */
        void onColorChosen(int color);
    }
}
