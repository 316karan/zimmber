package com.zimmber.ui.views;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.zimmber.models.DotEntity;
import com.zimmber.ui.interfaces.OnDrawingViewInteractionListener;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by karan on 2/6/16.
 */
public class DrawingView extends View {

    private static final String TAG = "DrawingView";
    private Paint mPaint;
    public int mRadius = 30,mDelta=10;
    private DotEntity mTempDot;
    private OnDrawingViewInteractionListener mListener;
    List<DotEntity> mDots;
    private Timer mTimer;

    public DrawingView(Context context) {
        super(context);
    }

    public DrawingView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DrawingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public DrawingView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    /**
     * initiate the view with required components
     * */
    public void init(int color, List<DotEntity> dots,OnDrawingViewInteractionListener listener){
        this.mDots = dots;
        this.mListener = listener;
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.FILL);
        changeColor(color);
        notifyDataSetChanged();
    }

    /**
     * called when the list of dots is changed. Sets the temporary object to null
     * to remove overdrawing
     * */
    public void notifyDataSetChanged(){
        invalidate();
        mTempDot = null;
    }

    /**
     * called when the a new color is selected at the activity level
     * */
    public void changeColor(int color){
        mPaint.setColor(color);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Log.d(TAG,"on draw called");
        if (mDots!=null){
            for (DotEntity dot : mDots){
                drawDot(canvas,dot);
            }
        }
        if (mTempDot!=null) {
            drawDot(canvas, mTempDot);
        }
    }

    /**
     * draw a dot on the canvas replicating a DotEntity
     * */
    private void drawDot(Canvas canvas,DotEntity dot){
        if (mPaint!=null) {
            mPaint.setColor(dot.getColor());
            canvas.drawCircle(dot.getLocX(),dot.getLocY(),dot.getRadius(),mPaint);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.d(TAG,"on touch called");
        switch (event.getAction()){
            case MotionEvent.ACTION_UP :
                Log.d(TAG, "on motion up called");
                invalidate();
                stopTimer();
                if (mTempDot!=null) {
                    mListener.onNewDotDrawn(mTempDot);
                }
                return true;
            case MotionEvent.ACTION_DOWN:
                Log.d(TAG,"on motion down called");
                mTempDot = new DotEntity(mPaint.getColor(),mRadius,event.getX(),event.getY());
                startTimer();
                return true;
            case MotionEvent.ACTION_MOVE:
                if (mTempDot!=null){
                    mTempDot.setLocX(event.getX());
                    mTempDot.setLocY(event.getY());
                }
                return true;
        }
        return false;
    }

    /**
     * start the timer once action down is called to increase the radius of the dot
     * */
    private void startTimer(){
        Log.d(TAG,"starting timer");
        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                ((Activity) getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onTimedTask();
                    }
                });
            }
        },300,300);
    }

    /**
     * increase the radius of the dot after defined milliseconds
     * */
    private void onTimedTask(){
        Log.d(TAG,"on timed task called");
        if (mTempDot!=null){
            Log.d(TAG,"radius : " + mTempDot.getRadius());
            mTempDot.setRadius(mTempDot.getRadius() + mDelta);
            Log.d(TAG, "new radius : " + mTempDot.getRadius());
            invalidate();
        }
    }

    /**
     * stop the timer on the action up event
     * */
    private void stopTimer(){
        Log.d(TAG,"stopping timer");
        mTimer.cancel();
        mTimer = null;
    }
}
