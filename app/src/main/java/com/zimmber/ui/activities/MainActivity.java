package com.zimmber.ui.activities;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.zimmber.R;
import com.zimmber.config.AppController;
import com.zimmber.config.constants.StorageVars;
import com.zimmber.controllers.DotController;
import com.zimmber.models.DotEntity;
import com.zimmber.ui.adapters.ColorListAdapter;
import com.zimmber.ui.interfaces.ControllerCallback;
import com.zimmber.ui.interfaces.OnDrawingViewInteractionListener;
import com.zimmber.ui.interfaces.VoidCallback;
import com.zimmber.ui.views.DrawingView;
import com.zimmber.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,ColorListAdapter.OnColorItemClickListener,OnDrawingViewInteractionListener{

    private RecyclerView mListColors;
    private View mBtnReset,mBtnUndo;
    private ColorListAdapter mColorAdapter;
    private DrawingView mDrawingView;
    private List<DotEntity> mDots = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mListColors = (RecyclerView) findViewById(R.id.list_colors);
        mBtnReset = findViewById(R.id.btn_reset);
        mBtnUndo = findViewById(R.id.btn_undo);
        mDrawingView = (DrawingView) findViewById(R.id.view_canvas);
        mListColors.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        mColorAdapter = new ColorListAdapter(this);
        mListColors.setAdapter(mColorAdapter);
        mDrawingView.init(Utils.getLastSelectedColorInt(), mDots, this);
        mBtnReset.setOnClickListener(this);
        mBtnUndo.setOnClickListener(this);
        initView();
    }

    @Override
    public void onColorChosen(int color) {
        mDrawingView.changeColor(color);
    }

    @Override
    public void onNewDotDrawn(final DotEntity dot) {
        DotController.getInstance().createDot(dot, new VoidCallback() {
            @Override
            public void onResponse() {
                mDots.add(dot);
                mDrawingView.notifyDataSetChanged();
            }

            @Override
            public void onFail(String reason) {
                showShortDurationSnackBar(reason);
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_reset:
                performResetAction();
                break;
            case R.id.btn_undo:
                performUndoAction();
                break;
        }
    }

    /**
     * reset all dots in database and repopulate view
     * */
    private void performResetAction(){
        DotController.getInstance().resetDots(new VoidCallback() {
            @Override
            public void onResponse() {
                AppController.getSessionManager().removeKey(StorageVars.LAST_SELECTED_COLOR);
                mDrawingView.changeColor(Utils.getLastSelectedColorInt());
                mColorAdapter.notifyDataSetChanged();
                initView();
            }

            @Override
            public void onFail(String reason) {
                showShortDurationSnackBar(reason);
            }
        });
    }

    /**
     * delete the last dot from the database and re populate view
     * */
    private void performUndoAction(){
        DotController.getInstance().deleteLastDot(new VoidCallback() {
            @Override
            public void onResponse() {
                initView();
            }

            @Override
            public void onFail(String reason) {
                showShortDurationSnackBar(reason);
            }
        });
    }

    /**
     * get all existing dots from database and populate view
     * */
    private void initView(){
        DotController.getInstance().getAllDots(new ControllerCallback<List<DotEntity>>() {
            @Override
            public void onResponse(List<DotEntity> response) {
                Log.d("response", "size : " + response.size());
                mDots.clear();
                mDots.addAll(response);
                mDrawingView.notifyDataSetChanged();
            }

            @Override
            public void onFail(String reason) {
                showShortDurationSnackBar(reason);
            }
        });
    }

    /**
     * show snack bar for short duration with
     * @param message
     * */
    public Snackbar showShortDurationSnackBar(String message) {
        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT);
        snackbar.show();
        return snackbar;
    }
}
