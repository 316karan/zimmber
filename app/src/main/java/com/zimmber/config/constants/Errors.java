package com.zimmber.config.constants;

/**
 * Created by karan on 2/6/16.
 */
public interface Errors {
    String ERROR_INSERT = "Could not insert object",ERROR_LOAD="Could not load data",ERROR_QUERY="Could not execute query";
}
