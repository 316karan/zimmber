package com.zimmber.config.constants;

/**
 * Created by karan on 2/6/16.
 */
public class StorageVars {

    private static final String prefix = "com.zimbber.pref.";
    public static final String LAST_SELECTED_COLOR = prefix + "last_selected_color";

}
