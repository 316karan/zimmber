package com.zimmber.config.constants;

/**
 * Created by karan on 2/6/16.
 */
public interface StaticVars {

    String PREF_NAME = "zimbber-pref";

    String[] COLORS = new String[]{
            "#000000",
            "#0000ff",
            "#00ff00",
            "#00ffff",
            "#ff0000",
            "#ff00ff",
            "#ffff00",
            "#ffffff",
            "#abcd12",
            "#dbca55"
    };
}
