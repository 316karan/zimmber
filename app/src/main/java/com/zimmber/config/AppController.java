package com.zimmber.config;

import android.app.Application;

import com.zimmber.database.DatabaseHelper;
import com.zimmber.utils.SessionManager;

/**
 * Created by karan on 2/6/16.
 */
public class AppController extends Application{

    private static AppController instance;
    private static SessionManager mSessionManager;
    private static DatabaseHelper mDBHelper;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        mSessionManager = SessionManager.getInstance(this);
        mDBHelper = DatabaseHelper.getInstance(this);
    }

    /**
     * get the singleton instance of the app
     * */
    public static AppController getInstance() {
        return instance;
    }

    /**
     * get the singleton instance of the shared preference manager
     * */
    public static SessionManager getSessionManager() {
        return mSessionManager;
    }

    /**
     * get the singleton instance of the database helper
     * */
    public static DatabaseHelper getDBHelper() {
        return mDBHelper;
    }

}
