package com.zimmber;

import android.content.Context;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.SmallTest;

import com.zimmber.config.constants.StaticVars;
import com.zimmber.config.constants.StorageVars;
import com.zimmber.utils.SessionManager;
import com.zimmber.utils.Utils;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;


/**
 * Created by karan on 2/6/16.
 */

@RunWith(AndroidJUnit4.class)
@SmallTest
public class SessionManagerTest {

    @Mock
    Context context;

    private SessionManager mSessionManager;

    /**
     * create instance of the SharedPreference Manager class
     * */
    @Before
    public void init(){
        mSessionManager = SessionManager.getInstance(context);
    }

    /**
     * test insert read and delete functions of the SharedPreference Manager class
     * */
    @Test
    public void sessionTest(){
        mSessionManager.clearSF();
        Assert.assertEquals(mSessionManager.getStringPref(StorageVars.LAST_SELECTED_COLOR, "abc"), "abc");
        mSessionManager.insertStringPref(StorageVars.LAST_SELECTED_COLOR, "pqr");
        Assert.assertEquals(mSessionManager.getStringPref(StorageVars.LAST_SELECTED_COLOR, "abc"), "pqr");
        Assert.assertEquals(mSessionManager.contains(StorageVars.LAST_SELECTED_COLOR), true);
        mSessionManager.removeKey(StorageVars.LAST_SELECTED_COLOR);
        Assert.assertEquals(mSessionManager.contains(StorageVars.LAST_SELECTED_COLOR), false);
    }
}
