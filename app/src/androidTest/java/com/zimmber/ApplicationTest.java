package com.zimmber;

import android.app.Application;
import android.test.ApplicationTestCase;

import com.zimmber.config.AppController;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<AppController> {
    public ApplicationTest() {
        super(AppController.class);
    }
}