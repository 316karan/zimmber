package com.zimmber;

import android.content.Context;
import android.support.test.runner.AndroidJUnit4;

import com.zimmber.controllers.interfaces.DBCallback;
import com.zimmber.database.DatabaseHelper;
import com.zimmber.database.DatabaseManager;
import com.zimmber.models.DotEntity;
import com.zimmber.ui.interfaces.VoidCallback;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * Created by karan on 2/6/16.
 */
@RunWith(AndroidJUnit4.class)
public class DatabaseTest {

    private DatabaseHelper mDBHelper;

    private DotEntity mDot;

    @Mock
    Context context;

    private DatabaseManager<DotEntity> mDBManager;
    private CountDownLatch latch = new CountDownLatch(1);

    /**
     * create mock objects and other required classes
     * */
    @Before
    public void init(){
        mDot = new DotEntity(0,0,0f,0f);
        mDBHelper = DatabaseHelper.getInstance(context);
        mDBManager = new DatabaseManager<>(mDBHelper,DotEntity.class);
    }

    /**
     * run the test function and check the count
     * */
    @Test
    public void insert(){
        mDBManager.insert(mDot, new DBCallback<Long>() {
            @Override
            public void onSuccess(Long response) {
                Assert.assertNotNull(response);
                read(1);
            }

            @Override
            public void onFail(String reason) {

            }
        });
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * check the count after inserting and deleting the values from the database
     * @param expected the expected count
     * */
    private void read(final int expected){
        mDBManager.findAll(new DBCallback<List<DotEntity>>() {
            @Override
            public void onSuccess(List<DotEntity> response) {
                Assert.assertNotNull(response);
                Assert.assertEquals(response.size(), expected);
                if (expected == 1) {
                    delete();
                }else{
                    latch.countDown();
                }
            }

            @Override
            public void onFail(String reason) {

            }
        });
    }

    /**
     * delete from database and check the count
     * */
    private void delete(){
        mDBManager.execRawQuery("Delete from " + DotEntity.DotColumns.TABLE_NAME, new VoidCallback() {
            @Override
            public void onResponse() {
                read(0);
            }

            @Override
            public void onFail(String reason) {

            }
        });
    }

}
